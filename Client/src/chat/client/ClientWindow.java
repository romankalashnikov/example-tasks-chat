package chat.client;

import chat.network.TCPConnection;
import chat.network.TCPConnectionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientWindow extends JFrame implements ActionListener,TCPConnectionListener,MouseListener {

    private static final String IP_ADDR = "192.168.1.4";
    private static final int PORT = 8622;
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientWindow();
            }
        });
    }

    private final String name = JOptionPane.showInputDialog("What is your name?");
    private  JTextArea log;
    private  JTextField fieldName;
    private  JTextField fieldinput;
    private  JButton sendButton;



    private TCPConnection connection;

    private String getNameOnChat(String name){
        while (name.equals(""))
            name =  JOptionPane.showInputDialog("What is your name?");
            setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        return name;
    }

    private String getTime() {
             Date date = new Date();
             SimpleDateFormat sDF = new SimpleDateFormat("HH:mm:ss");
             String time = sDF.format(date);
            return time;
    }

    private ClientWindow(){
        JFrame frame = new JFrame("Chat Frame");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(WIDTH, HEIGHT);

        JPanel panel = new JPanel(); // the panel is not visible in output

        log = new JTextArea();
        log.setEditable(false);
        fieldName = new JTextField(getNameOnChat(name));
        fieldName.setEditable(false);
        fieldinput = new JTextField();

        fieldinput.addActionListener(this);
        sendButton = new JButton("Send");
        sendButton.addMouseListener(this);


        panel.add(fieldName);
        panel.add(fieldinput);
        panel.add(log);

        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.getContentPane().add(BorderLayout.SOUTH, fieldinput);
        frame.getContentPane().add(BorderLayout.AFTER_LINE_ENDS, sendButton);
        frame.getContentPane().add(BorderLayout.NORTH, fieldName);
        frame.getContentPane().add(BorderLayout.CENTER, log);

        frame.getRootPane().setDefaultButton(sendButton);

        frame.setVisible(true);

        frame.setLocationRelativeTo(null);
        frame.setAlwaysOnTop(true);

        try {
            connection = new TCPConnection(this, IP_ADDR,PORT);
        } catch (IOException e) {
            printMessage("Connection exception: " + e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String msg = fieldinput.getText();
        if(msg.equals(""))return;
        fieldinput.setText(null);
        connection.sendString(getTime() + "  " + fieldName.getText() + " : " + msg);
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMessage("Connection ready...." );
    }

    @Override
    public void onReaceivString(TCPConnection tcpConnection, String value) {
        printMessage(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMessage("Connection close...");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        printMessage("Connection exception: " + e);
    }

    private synchronized void printMessage(String msg){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(msg + "\n");
                log.setCaretPosition(log.getDocument().getLength());
            }
        });
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getButton()==MouseEvent.BUTTON1){
            String msg = fieldinput.getText();
            if(msg.equals(""))return;
            fieldinput.setText(null);
            connection.sendString(getTime() + "  " + fieldName.getText() + " : " + msg);

        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
