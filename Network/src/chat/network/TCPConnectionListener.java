package chat.network;

public interface TCPConnectionListener {

    void onConnectionReady(TCPConnection tcpConnection);
    /**
     * Запущено соединение, можно работать
     */

    void onReaceivString(TCPConnection tcpConnection, String value);
    /**
     * СОединение приняло входящую строчку
     */

    void onDisconnect(TCPConnection tcpConnection);
    /**
     * Соединение прервалось
     */
    void onException(TCPConnection tcpConnection, Exception e);
    /**
     * Появилось Исключение
     */
}
