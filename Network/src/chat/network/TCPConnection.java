package chat.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

public class TCPConnection {
    private final Socket socket;
    private final Thread rxThread;
    private final TCPConnectionListener eventListiner;
    private final BufferedReader in;
    private final BufferedWriter out;

    public TCPConnection(TCPConnectionListener eventListiner, String ipAddr, int port) throws IOException{
        /**
         * Передали коструктор TCPCOnnection(eventListiner, socet)
         */
        this(eventListiner, new Socket(ipAddr,port));
    }



    public TCPConnection(TCPConnectionListener eventListiner, Socket socket) throws IOException{
        this.eventListiner = eventListiner;
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader( socket.getInputStream(), Charset.forName("UTF-8")));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
        rxThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    /**
                     * Передал экземпляр обрамляющего класса(TCPConnection.this
                     */
                    eventListiner.onConnectionReady(TCPConnection.this);
                    while (!rxThread.isInterrupted()){
                        String message = in.readLine();
                        eventListiner.onReaceivString(TCPConnection.this, message);
                    }
                } catch (IOException e) {
                    eventListiner.onException(TCPConnection.this, e);
                }finally {
                    eventListiner.onDisconnect(TCPConnection.this);
                }
            }
        });
        rxThread.start();
    }

    public synchronized void sendString(String value){
        try {
            out.write(value + "\r\n");
            out.flush();
        } catch (IOException e) {
            eventListiner.onException(TCPConnection.this, e);
            disconnectStream();
        }
    }

    public synchronized void disconnectStream(){
        rxThread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListiner.onException(TCPConnection.this, e);
        }
    }

    @Override
    public String toString() {
        return "TCPConnection: "+ socket.getInetAddress()+ ": " + socket.getPort();
    }
}
